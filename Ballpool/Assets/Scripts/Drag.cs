﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script handles the drag and drop behaviour
/// </summary>
public class Drag : MonoBehaviour
{
    public bool draggingBall = false;
    public GameObject draggedObject;
    private Vector2 touchOffset;

    private BallPhysics ballPhysics;

    private bool HasInput
    {
        get => Input.GetMouseButton(1);
    }

    private void Update()
    {
        if(HasInput)
        {
            DragBall();
        }
        else
        {
            if(draggingBall)
                DropBall();
        }
    }

    // get mouse position
    Vector2 CurrentTouchPosition
    {
        get
        {
            Vector2 inputPos;
            inputPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            return inputPos;
        }
    }

    private void DragBall()
    {
        Vector2 inputPosition = CurrentTouchPosition;

        // if the user is dragging the ball, it will transform its position to mouseposition
        if (draggingBall)
        {
            draggedObject.transform.position = inputPosition + touchOffset;
        }
        
        else
        {
            RaycastHit2D[] touches = Physics2D.RaycastAll(inputPosition, inputPosition, 0.5f);
            if (touches.Length > 0)
            {
                var hit = touches[0];
                if (hit.transform != null)
                {
                    draggingBall = true;
                    draggedObject = hit.transform.gameObject;
                    ballPhysics = draggedObject.GetComponent<BallPhysics>();
                    touchOffset = (Vector2)hit.transform.position - inputPosition;
                    ballPhysics.isBeingDragged = true;
                }
            }
        }
    }

    private void DropBall()
    {
        draggingBall = false;
        ballPhysics.isBeingDragged = false;
    }
}
