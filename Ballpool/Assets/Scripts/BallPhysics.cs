﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script handles the physics of each ball that is instantiated by the user
/// </summary>
public class BallPhysics : MonoBehaviour
{
    public Rigidbody2D rb;
    private float thrust;
    private float bounceForce;
    public bool isBeingDragged;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        thrust = 0.5f;
        bounceForce = 1f;
    }


    void FixedUpdate()
    {
        // causese the balls to not just fall down without any velocity
        rb.AddForce(new Vector2(transform.position.x, transform.position.y) * thrust);
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Border"))
            thrust = 0.5f;
        else
            thrust = 0f;

        // checks if this ball is hitting the dragged ball, which causes this ball to bounce off of the dragged ball
        if (other.gameObject.CompareTag("Ball"))
        {
            BallPhysics otherBallPhysics = other.gameObject.GetComponent<BallPhysics>();
            if (otherBallPhysics.isBeingDragged)
            {
                rb.AddForce(new Vector2(transform.position.x , transform.position.y) * bounceForce, ForceMode2D.Impulse);
            }
        }
    }

}
