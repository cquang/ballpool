﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    //ball object and list
    [SerializeField] private GameObject ball;
    private GameObject objectSpawn;
    private List<GameObject> ballsList;
    private bool isDestroyed;

    //customization ball
    private Sprite[] spriteArray;
    private int randomSprite;
    [SerializeField] private int maxBallsOnScreen;
    [SerializeField] private float minScale;
    [SerializeField] private float maxScale;

    //double clicking
    private float lastClickTime;
    private float catchTime = 0.25f;

    private void Awake()
    {
        spriteArray = Resources.LoadAll<Sprite>("Sprites");
    }
    void Start()
    {
        ballsList = new List<GameObject>();

        //ball customization
        maxBallsOnScreen = 130;
        minScale = 1f;
        maxScale = 2.2f;
    }
    
    void FixedUpdate()
    {
        // hold left mouse click to spawn
        if (Input.GetMouseButton(0))
        {
            StartCoroutine("Spawn");
            ballsList.Add(objectSpawn);
        }


        //double click to reset and destroy all balls
        if (Input.GetMouseButtonDown(0))
        {
            if (Time.time - lastClickTime < catchTime)
            {
                for (int i = 0; i < ballsList.Count; i++)
                {
                    Destroy(ballsList[i]);
                    isDestroyed = true;
                }
            }
            lastClickTime = Time.time;
        }

        // destroy ball game objects if there are too many
            for (int i = 0; i < ballsList.Count; i++)
        {
            if (ballsList.Count > maxBallsOnScreen)
            {
                Destroy(ballsList[i]);
                isDestroyed = true;
            }
        }

        // clear list after destroying the balls
        if (isDestroyed)
        {
            ballsList.Clear();
            isDestroyed = false;
        }
    }

    // using ienumerator because otherwise
    IEnumerator Spawn()
    {

        randomSprite = Random.Range(0, spriteArray.Length);

        Vector2 spawnPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        objectSpawn = Instantiate(ball, spawnPosition, Quaternion.identity);

        // size of balls
        objectSpawn.transform.localScale = Vector2.one * Random.Range(minScale, maxScale);
        // randomize sprites on the spawned balls
        objectSpawn.GetComponent<SpriteRenderer>().sprite = spriteArray[randomSprite];
        // add collider to sprite that adjusts the collider radius based on the gameobject scale
        objectSpawn.AddComponent<CircleCollider2D>();

        yield return new WaitForSeconds(0.3f);
    }
}
